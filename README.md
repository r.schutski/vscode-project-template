# VSCode project template

This is a template for projects of services / streamlit apps.

## Setup (of the service you develop)

To install dependencies run in the root of the repository

```bash
poetry install
<your service start command>
```

Alternatively, you can use docker to build and run the image:

```bash
docker-compose up -d
```

This will start Streamlit on port 8515.
Remove the containers with 

```bash
docker-compose down
```

## Development

This repository provides a Devcontainer image for development. To run the image, open the repository
root in Visual Studio Code, launch command palette (Ctrl + Shift + P)
and run `Remote-Containers: Reopen in Container`.

The container will mount the repository root to `/code` directory. To change this behavior, modify
`.devcontainer/docker-compose.yml` and the `"workspaceFolder"` setting in
`.devcontainer/devcontainer.json`.

The development dependencies are listed in the `[tool.poetry.group.dev.dependencies]` section
of `pyproject.toml`

### Updating dependencies

This repository uses Poetry to manage dependencies and to build package.
Use `poetry add XXXX` to add new dependencies and `poetry update` to update existing dependencies.
When the contents of `pyproject.toml` are updated manually, you need to run `poetry lock` to synchronize
the lock file with your requirements. Make sure you commit both `pyproject.toml` and `poetry.lock`.

Poetry allows to fuzzy fix the version with the `^` symbol. This means that the resolver will use 
newer package which is compatible. Strict fixing will be done if you remove `^`. Check `pyproject.toml`
and/or poetry documentation for more examples.

The dependencies in `pyproject.toml` are separated into 3 groups: main, dev and local. This is custom,
you can define your groups yourself. The service container is run with the dependencies of the main 
group, and the dev and local dependencies are used only in the Devcontainer. Make sure you rebuild 
the service continer if you updade main dependencies. You can do it with `Remote-Containers: Rebuild Container`
command of Visual Studio Code. 

## License

Copyright of Insilico Medicine (2024)

## Project status

Development start: May 2024
